using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RentMovie.Data;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using RentMovie.Models;

namespace RentMovie
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var server=Configuration["DBServer"]??"ms-sql-server";
            var port=Configuration["DBPort"]??"1433";
            var user=Configuration["DBUser"]??"sa";
            var password=Configuration["DBPassword"]??"Qwerty1234@";
            var database=Configuration["Database"]??"RentMovieDb";
           
            services.AddDbContext<MovieContext>(opt=> opt.UseSqlServer($"Server={server},{port};Initial Catalog={database}; User ID={user}; Password={password};"));
            services.AddControllers().AddNewtonsoftJson(s=>{
                s.SerializerSettings.ContractResolver=new CamelCasePropertyNamesContractResolver();
            });
            System.Console.WriteLine(services);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IMovieRepo, SqlMovieRepo>(); 
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RentMovie", Version = "v1" });
            });       }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RentMovie v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            PrepDB.PrepPopulation(app);
        }
    }
}
