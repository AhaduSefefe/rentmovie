using AutoMapper;
using RentMovie.Dtos;
using RentMovie.Models;

namespace RentMovie.Profiles
{
    public class MoviesProfile: Profile{
        public MoviesProfile()
        {
            //Source->Target
            CreateMap<Movie,MovieReadDto>();
            CreateMap<MovieCreateDto,Movie>();
            CreateMap<MovieUpdateDto, Movie>();
            CreateMap<Movie,MovieUpdateDto>();
        }
    }
}