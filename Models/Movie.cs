using System.ComponentModel.DataAnnotations;

namespace RentMovie.Models
{
    public class Movie{
        [Key]
        public int Id {get;set;}

        [Required]
        [MaxLength(250)]
        public string Name {get;set;}

        [Required]
        public string Producer {get;set;}
        
        [Required]
        public int Rating {get;set;}
    }
}