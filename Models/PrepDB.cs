using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RentMovie.Data;
using System.Linq;

namespace RentMovie.Models
{
    public static class PrepDB{
        public static void PrepPopulation(IApplicationBuilder app){
            using (var serviceScope=app.ApplicationServices.CreateScope()){
                SeedData(serviceScope.ServiceProvider.GetService<MovieContext>());
            }
        }
        public static void SeedData(MovieContext context){
            System.Console.WriteLine("Applying Migration...");
            context.Database.Migrate();

            if(!context.Movies.Any()){
                System.Console.WriteLine("Adding Data-seeding...");
                context.Movies.AddRange(
                    new Movie() {Name="qwerty",Producer="qwerty",Rating=5}
                );
            }
            else{
                System.Console.WriteLine("Already have data");
            }
        }
    }
}