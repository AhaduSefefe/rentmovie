using Microsoft.EntityFrameworkCore;
using RentMovie.Models;

namespace RentMovie.Data{
    
    public class MovieContext: DbContext
    {
        public MovieContext(DbContextOptions<MovieContext> opt):base(opt)
        {
            
        }
        
        public DbSet<Movie> Movies { get; set; }
    }
}