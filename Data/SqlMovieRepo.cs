using System;
using System.Collections.Generic;
using System.Linq;
using RentMovie.Models;

namespace RentMovie.Data
{
    public class SqlMovieRepo : IMovieRepo
    {
        private readonly MovieContext _context;

        public SqlMovieRepo(MovieContext context)
        {
            _context=context;
        }

        public void CreateMovie(Movie movie)
        {
            if(movie==null){
                throw new ArgumentNullException(nameof(movie));
            }
            _context.Movies.Add(movie);
        }

        public Movie GetMovieById(int id)
        {
            return _context.Movies.FirstOrDefault(p=>p.Id==id);
        }

        public IEnumerable<Movie> GetMovies()
        {
         
            return _context.Movies.ToList();
        }

        public bool SaveChanges()
        {
            return(_context.SaveChanges()>=0);
        }

        public void UpdateMovie(Movie movie)
        {
            
        }
        
    }
}