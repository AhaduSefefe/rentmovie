using System.Collections.Generic;
using RentMovie.Models;

namespace RentMovie.Data
{
    public interface IMovieRepo
    {
        bool SaveChanges();
        IEnumerable<Movie> GetMovies();
        Movie GetMovieById(int id);
        void CreateMovie(Movie movie);
        void UpdateMovie(Movie movie);
    
    }
}