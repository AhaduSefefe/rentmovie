
using System.ComponentModel.DataAnnotations;

namespace RentMovie.Dtos
{
    public class MovieCreateDto{
        [Required]
        [MaxLength(250)]
        public string Name {get;set;}

        [Required]
        public string Producer {get;set;}
        
        [Required]
        public int Rating {get;set;}
    }
}