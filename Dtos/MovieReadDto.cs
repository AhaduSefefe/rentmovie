
namespace RentMovie.Dtos
{
    public class MovieReadDto{
        public int Id {get;set;}
        public string Name {get;set;}
        public int Rating {get;set;}
    }
}