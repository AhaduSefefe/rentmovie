
using System.ComponentModel.DataAnnotations;

namespace RentMovie.Dtos
{
    public class MovieUpdateDto{
        [MaxLength(250)]
        public string Name {get;set;}

        public string Producer {get;set;}
        
        public int Rating {get;set;}
    }
}