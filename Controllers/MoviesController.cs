using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using RentMovie.Data;
using RentMovie.Dtos;
using RentMovie.Models;
using System;

namespace RentMovie.Controllers{
[Route("api/movies")]
[ApiController]
    public class MoviesController : ControllerBase{
        private readonly IMovieRepo _repository;
        private readonly IMapper _mapper;

        public MoviesController(IMovieRepo repository,IMapper mapper){
                _repository=repository;
                _mapper=mapper;
        }
        
        
        //GET api/movies/
        [HttpGet]
        public ActionResult <IEnumerable<MovieReadDto>> GetAllMovies(){
            var movies=_repository.GetMovies();
            return Ok(_mapper.Map<IEnumerable<MovieReadDto>>(movies));

        }

        //GET api/movies/{id}
        [HttpGet("{id}", Name ="GetMovieById")]
        public ActionResult <MovieReadDto> GetMovieById(int id){
            var movie= _repository.GetMovieById(id);
            if(movie!=null){
            return Ok(_mapper.Map<MovieReadDto>(movie));}
            else
            return NotFound();
        }

        //POST api/movies
        [HttpPost]
        public ActionResult <MovieReadDto> CreateMovie(MovieCreateDto movieCreateDto){
            var movieModel= _mapper.Map<Movie>(movieCreateDto);
            _repository.CreateMovie(movieModel);
            _repository.SaveChanges();

            var movieReadDto=_mapper.Map<MovieReadDto>(movieModel);
            
            return CreatedAtRoute(nameof(GetMovieById),new {Id=movieReadDto.Id},movieReadDto );
        }

        //PUT api/movies/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateMovie(int id, MovieUpdateDto movieUpdateDto){
            var movieModelFromRepo= _repository.GetMovieById(id);
            if(movieModelFromRepo==null){
                return NotFound();
            }

            _mapper.Map(movieUpdateDto,movieModelFromRepo);
            _repository.UpdateMovie(movieModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
        }

        //PATCH api/commands/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialCommandUpdate(int id,[FromBody] JsonPatchDocument<MovieUpdateDto> patchDoc){
            if(patchDoc!=null){
                var movieModelFromRepo= _repository.GetMovieById(id);
                        if(movieModelFromRepo==null){
                            return NotFound();
                        }
            var movieToPatch=_mapper.Map<MovieUpdateDto>(movieModelFromRepo);
            patchDoc.ApplyTo(movieToPatch,ModelState);
            if(!TryValidateModel(movieToPatch)){
                 
                return  ValidationProblem(ModelState);
            }

            _mapper.Map(movieToPatch,movieModelFromRepo);
            _repository.UpdateMovie(movieModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
            }
            else{
                return BadRequest();
            }
        }
    }
}